# The Pokemon Game
Endpoints allow the single player to wander around until a wild pokemon appears. If a pokemon appears the player has a few chances to attempt to capture the pokemon. If the player exceeds the attempts at the pokemon the encounter ends. If the player captures the pokemon it will be recorded. 

# Getting Started
* SpringBoot
* Maven
* Java 8

Project uses [MariaDB4j](https://github.com/vorburger/MariaDB4j) as a persistent database. The database is written to `data/local`. Unit tests that use MariaDB4j will write to a new temporary directory each time. 

To package: `./mvnw package -Dmaven.test.skip=true`
To test: `./mvnw test`
To run: `java -jar target/*.jar`
To clear database: `rm -rf data/local`


# Troubleshooting
Issues:
1. OpenSSL Error on Mac
    
    dyld: Library not loaded: /usr/local/opt/openssl/lib/libssl.1.0.0.dylib Referenced from: /private/var/folders/w6/_r897gfx27b3bqj9_zvckvth0000gn/T/MariaDB4j/base/bin/my_print_defaults Reason: image not found
    
    Fix: Instead of reverting to a previous version of openssl. Follow  https://github.com/vorburger/MariaDB4j/issues/48#issuecomment-623078691

### Reference Documentation
For further reference, please consider the following sections:

* [PokeAPI](https://pokeapi.co/)
* [Confluence Document](https://contrast.atlassian.net/wiki/spaces/TS/pages/1543077895/Backend+Interview+Project)

