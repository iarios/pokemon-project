package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.CapturedPokemon;
import com.pokemon.pokemondemo.DB.EncounterModel;
import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.DB.CapturedPokemonRepository;
import com.pokemon.pokemondemo.DB.EncounterRepository;
import com.pokemon.pokemondemo.DB.PokemonRepository;
import helper.HelperRandom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pokemonService.CloneService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Endpoint that allows
 * - the user to wander the world to encounter a pokemon
 * - the user to attempt to capture a pokemon
 */
@Slf4j
@RestController
public class AdventureRestController {

    @Autowired
    PokemonRepository pokemonRepository;

    @Autowired
    EncounterRepository encounterRepository;

    @Autowired
    CapturedPokemonRepository capturedPokemonRepository;


    /**
     * Wandering endpoint that can trigger an encounter.
     * @return
     */

   /* @RequestMapping("/clone")
    @ResponseBody
    public PokemonResponse clone() {

        CloneResponse response = new CloneResponse();
        PokemonResponse res = new PokemonResponse();

        res.setId(1);
        res.setName("Ditto");
        res.setWeight(12);
        res.setHeight(2);
        response.getId();
        response.getName();
        response.getWeight();
        response.getHeight();

        res.setClone(response);
        return res;
    }
*/
    @RequestMapping("/cloned")
    @ResponseBody
    public PokemonResponse clone() {

        Long maxId = pokemonRepository.findMaxId();
        Long randomPokemonId = ThreadLocalRandom.current().nextLong(1, maxId) + 1;
        PokemonModel pokemon = pokemonRepository.findById(randomPokemonId).get();

        CloneService response= new CloneService();

        return response.cloneR(pokemon);

    }

    @RequestMapping("/encounter")
    @ResponseBody
    public EncounterResponse encounter() {
        EncounterResponse response = new EncounterResponse();
        boolean random = HelperRandom.getRandomBoolean();
        if (random) {
            response.setEncounter(true);
            response.setEncounterToken(getShortUUID());
            Long maxId = pokemonRepository.findMaxId();

            Long randomPokemonId = ThreadLocalRandom.current().nextLong(1, maxId) + 1;
            PokemonModel pokemon = pokemonRepository.findById(randomPokemonId).get();
            response.setPokemon(pokemon);

            EncounterModel encounterModel = new EncounterModel();
            encounterModel.setPokemon(pokemon);
            encounterModel.setHash(response.getEncounterToken());
            encounterRepository.save(encounterModel);
        }
        return response;
    }

    /**
     * Allows the player an attempt to capture the encountered pokemon
     * @param encounter_token
     * @return
     */
    @RequestMapping("/encounter/capture/{encounter_token}")
    @ResponseBody
    public CaptureResponse capture(@PathVariable String encounter_token) {
        EncounterModel encounter = encounterRepository.findByHash(encounter_token);
        if(encounter != null) {
            CaptureResponse captureResponse = new CaptureResponse();
            int attempts = encounter.getCaptureAttempts();
            int totalAttempts = attempts + 1;
            if (attempts < 3) {
                if(HelperRandom.getRandomBoolean()) {
                    CapturedPokemon capturedPokemon = new CapturedPokemon();
                    capturedPokemon.setPokemon(encounter.getPokemon());
                    capturedPokemonRepository.save(capturedPokemon);
                    encounterRepository.delete(encounter);
                    captureResponse.setRemainingAttempts(0);
                    captureResponse.setCaptured(true);
                    captureResponse.setPokemon(encounter.getPokemon());
                } else {
                    encounter.setCaptureAttempts(totalAttempts);
                    encounterRepository.save(encounter);
                    captureResponse.setRemainingAttempts(3 - totalAttempts);
                }
            } else {
                encounterRepository.delete(encounter);
                throw new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "Encounter not found"
                );
            }

            return captureResponse;
        }
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Encounter not found"
        );
    }

    @RequestMapping("/poke")
    @ResponseBody
    public EncounterResponse listpokemon() {
        EncounterResponse response = new EncounterResponse();
        boolean random = HelperRandom.getRandomBoolean();
        Long maxId = pokemonRepository.findMaxId();
        List<PokemonModel> listpo= new ArrayList();

        for(int i=0; i<=maxId; i++){

            PokemonModel pokemon = pokemonRepository.findById(i);
            response.setPokemon(pokemon);
            listpo.add(pokemon);

              }

        System.out.println(listpo);
        return response;
        //return listpo;
    }



    /**
     * Get 8 Character UUID.
     *
     * @return
     */
    public String getShortUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().substring(0, 7);
    }
}
