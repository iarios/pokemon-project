package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.PokemonModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CaptureResponse {
    boolean captured;
    PokemonModel pokemon;
    int remainingAttempts;
}
