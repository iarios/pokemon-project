package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.PokemonModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EncounterResponse {
    private boolean encounter = false;
    private String encounterToken;
    private PokemonModel pokemon;
}
