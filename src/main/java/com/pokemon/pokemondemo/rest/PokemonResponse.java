package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.PokemonModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PokemonResponse {

   /* int id;
    private String name;
    private Integer weight;
    private Integer height;*/
    CloneResponse clone;
    PokemonModel pokemon;
}
