package com.pokemon.pokemondemo.rest;

import com.pokemon.pokemondemo.DB.PokemonModel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
//@Getter
//@Setter
public class CloneResponse {

    int id=2;
    private String name="Pikachu";
    private Integer weight=2;
    private Integer height=12;
    //PokemonModel pokemon;

}
