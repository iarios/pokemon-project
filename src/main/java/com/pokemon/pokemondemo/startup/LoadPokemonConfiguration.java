package com.pokemon.pokemondemo.startup;

import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.DB.PokemonRepository;
import lombok.extern.slf4j.Slf4j;
import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;
import java.util.stream.StreamSupport;

@Slf4j
@Configuration
@Profile({"default"})
public class LoadPokemonConfiguration {
    final private PokemonRepository repository;

    final private int maxLoad;

    public LoadPokemonConfiguration(PokemonRepository repository, @Value("${app.maxLoad}") int maxLoad) {
        this.repository = repository;
        this.maxLoad = maxLoad;
    }

    @PostConstruct
    public void startup() {
        PokeApi pokeApi = new PokeApiClient();

        if (StreamSupport.stream(repository.findAll().spliterator(), false).count() == 0) {
            for (int i = 1; i < maxLoad; i++) {
                Pokemon pokemon = pokeApi.getPokemon(i);
                PokemonModel pokemonModel = new PokemonModel();
                pokemonModel.setName(pokemon.getName());
                pokemonModel.setHeight(pokemon.getWeight());
                pokemonModel.setWeight(pokemon.getHeight());
                repository.save(pokemonModel);
            }
        }
    }

}
