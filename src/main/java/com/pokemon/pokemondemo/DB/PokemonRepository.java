package com.pokemon.pokemondemo.DB;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PokemonRepository extends CrudRepository<PokemonModel, Long> {
    List<PokemonModel> findByName(String lastName);
    PokemonModel findById(long id);

    @Query("select max(id) from pokemon")
    public Long findMaxId();

}


