package com.pokemon.pokemondemo.DB;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class CapturedPokemon {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;


    @OneToOne
    @JoinColumn(name = "pokemon_id", referencedColumnName = "id")
    private PokemonModel pokemon;

}
