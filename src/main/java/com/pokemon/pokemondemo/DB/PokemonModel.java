package com.pokemon.pokemondemo.DB;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "pokemon")
public class PokemonModel {
    private @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;
    private String name;
    private Integer weight;
    private Integer height;

}
