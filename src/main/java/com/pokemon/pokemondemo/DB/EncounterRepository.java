package com.pokemon.pokemondemo.DB;

import com.pokemon.pokemondemo.DB.EncounterModel;
import org.springframework.data.repository.CrudRepository;

public interface EncounterRepository extends CrudRepository<EncounterModel, Long> {
    EncounterModel findByHash(String hash);
}


