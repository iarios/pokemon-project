package com.pokemon.pokemondemo.DB;

import com.pokemon.pokemondemo.DB.CapturedPokemon;
import org.springframework.data.repository.CrudRepository;

public interface CapturedPokemonRepository extends CrudRepository<CapturedPokemon, Long> {
}


