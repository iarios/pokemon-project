package com.pokemon.pokemondemo.DB;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "encounters")
public class EncounterModel {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String hash;

    private Integer captureAttempts = 0;

    @OneToOne
    @JoinColumn(name = "pokemon_id", referencedColumnName = "id")
    private PokemonModel pokemon;


}
