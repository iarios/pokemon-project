package pokemonService;

import com.pokemon.pokemondemo.DB.PokemonModel;
import com.pokemon.pokemondemo.DB.PokemonRepository;
import com.pokemon.pokemondemo.rest.CloneResponse;
import com.pokemon.pokemondemo.rest.PokemonResponse;
import helper.HelperRandom;
import org.springframework.beans.factory.annotation.Autowired;



public class CloneService {

    @Autowired
    PokemonRepository pokemonRepository;

    public PokemonResponse cloneR(PokemonModel pokemon) {

        PokemonResponse response = new PokemonResponse();
        CloneResponse res = new CloneResponse();
        boolean random = HelperRandom.getRandomBoolean();

        if (random) {

            res.getId();
            res.getName();
            res.getWeight();
            res.getHeight();
            response.setClone(res);
            response.setPokemon(pokemon);

        }
        return response;
    }


}
